# Gets the Mullvad relay list
# Returns : 0 in case of success; non-zero code otherwise
# Prints : the relay list in case of success; the error message otherwise
relay_list() {
    local id
    id=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 12)
    curl --silent https://api.mullvad.net/rpc/ \
        --header 'Content-Type: application/json' \
        --data '{"jsonrpc":"2.0","id":"'"${id}"'","method":"relay_list_v3","params":[]}' \
        | jq -cer 'if .result != null then .result else (.error.code as $err | .error.message | halt_error($err)) end'
}

# Creates a Mullvad account
# Returns : 0 in case of success; non-zero code otherwise
# Prints : the account number in case of success
create_account() {
    curl --silent --fail --cookie-jar /dev/null --location https://mullvad.net/en/account/create/ 2>/dev/null \
        | hxclean 2>/dev/null \
        | hxselect -c '#account-ticket-container h1' \
        | tr -d ' '
}

# Gets the expiry date of an account
# Parameters :
#   account_number : Mullvad account number
# Returns : 0 in case of success; non-zero code otherwise
# Prints : the expiry date in case of success; the error message otherwise
get_expiry() {
    local account_number=${1:?Missing account number}
    local id
    id=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 12)
    curl --silent https://api.mullvad.net/rpc/ \
        --header 'Content-Type: application/json' \
        --data '{"jsonrpc":"2.0","id":"'"${id}"'","method":"get_expiry","params":["'"${account_number}"'"]}' \
        | jq -cer 'if .result != null then .result else (.error.code as $err | .error.message | halt_error($err)) end'
}

# Submits a voucher
# Parameters :
#   account_number : Mullvad account number
#   voucher : A Mullvad voucher
# Returns : 0 if the voucher is valid; 1 otherwise
# Prints : the returned code
submit_voucher() {
    local account_number=${1:?Missing account number}
    local voucher=${2:?Missing voucher code}
    curl --silent --request POST "https://api.mullvad.net/www/payments/submit-voucher/" \
        --header "Authorization: Token ${account_number}" \
        --data-urlencode "voucher_code=${voucher}" \
        | jq -cer 'if .code == "INVALID_VOUCHER" or .code == "VOUCHER_USED" then (.code | halt_error(1)) else .code end'
    echo
}

# Adds a port
# Parameters :
#   account_number : Mullvad account number
#   pubkey : A WireGuard public key. If not provided, a global port will be added
# Returns : 1 in case of failure; 0 otherwise
add_port() {
    local account_number=${1:?Missing account number}
    local pubkey=$2
    local args=()
    if [ -n "${pubkey+x}" ]; then
        args+=('--data-urlencode')
        args+=("pubkey=${pubkey}")
    fi
    curl --silent --fail --request POST "https://api.mullvad.net/accounts/${account_number}/add_port/" \
        "${args[@]}"
}

# Removes a port
# Parameters :
#   account_number : Mullvad account number
#   port : The port number to remove
#   pubkey : A WireGuard public key. If not provided, a global port will be deleted
# Returns : 1 in case of failure; 0 otherwise
remove_port() {
    local account_number=${1:?Missing account number}
    local port=${2:?Missing port number}
    local pubkey=$3
    local args=()
    if [ -n "${pubkey+x}" ]; then
        args+=('--data-urlencode')
        args+=("pubkey=${pubkey}")
    fi
    curl --silent --fail --request POST "https://api.mullvad.net/accounts/${account_number}/remove_port/" \
        --data-urlencode "port=${port}" \
        "${args[@]}"
}

# Adds a WireGuard public keys
# Parameters :
#   account_number : Mullvad account number
#   pubkey : A WireGuard public key. If not provided, a new one will be generated
# Returns : 1 in case of failure; 0 otherwise
add_wg() {
    local account_number=${1:?Missing account number}
    local pubkey=${2:-$(wg genkey)}
    curl --silent --fail --request POST "https://api.mullvad.net/wg/" \
        --data-urlencode "account=${account_number}" \
        --data-urlencode "pubkey=${pubkey}"
}

# Removes a WireGuard public keys
# Returns 1 in case of failure; 0 otherwise
remove_wg() {
    local account_number=${1:?Missing account number}
    local pubkey=${2:?Missing pubkey}
    curl --silent --fail --request POST "https://api.mullvad.net/accounts/${account_number}/revoke_wg_pubkey/" \
        --data-urlencode "pubkey=${pubkey}"
}

# Get a list of WireGuard public keys and associated ports
# Returns : 1 in case of failure; 0 otherwise
# Prints : the result of the Mullvad wg_pubkeys API call
wg_pubkeys() {
    local account_number=${1:?Missing account number}
    curl --silent --fail --request GET "https://api.mullvad.net/accounts/${account_number}/wg_pubkeys/"
}

# Test whether Mullvad VPN is used
# Returns : 0 if Mullvad VPN is used; 1 otherwise
# Prints : the result of the am.i.mullvad json API call
am_i_mullvad() {
    curl --silent https://am.i.mullvad.net/json \
        | jq -cer 'if .mullvad_exit_ip == false then (. | halt_error(1)) else . end'
}

# Test whether Mullvad DNS is used
# Returns : 0 if Mullvad DNS is used; 1 otherwise
# Prints : the result of the am.i.mullvad dnsleak API call
dnsleak_test() {
    local uuid
    uuid=$(uuidgen | tr -d '-')
    curl --silent "https://${uuid}.dnsleak.am.i.mullvad.net" 2>/dev/null
    curl --silent "https://am.i.mullvad.net/dnsleak/${uuid}" \
        | jq -cer 'if length != 1 or .[0].mullvad_dns != true then (. | halt_error(1)) else . end'
}
