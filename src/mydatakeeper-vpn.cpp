#include <csignal>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <set>

#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

#include <mydatakeeper/arguments.h>
#include <mydatakeeper/exec.h>

#include <mydatakeeper/config.h>
#include <mydatakeeper/applications.h>
#include <mydatakeeper/application.h>
#include <mydatakeeper/systemd.h>

#include "mullvad-proxy.h"

using namespace std;

// Child PID
pid_t pid;

DBus::BusDispatcher dispatcher;

// VPN config files
string config_file;
string credentials_file;

// Mullvad config files
string mullvad_ca_file;
string mullvad_relays_file;

// Global config values
string username;
string password;
string location;
uint64_t expiry_date;
map<string, uint32_t> relays_ip;
set<uint16_t> udp_ports;
set<uint16_t> tcp_ports;
string protocol;
string port;

void setup()
{
    clog << "Setting up VPN" << endl;

    string err;
    if (execute("/usr/lib/mydatakeeper-vpn/iptables-update", {"add"}, nullptr, &err) != 0) {
        cerr << "/usr/lib/mydatakeeper-vpn/iptables-update: " << err << endl;
        exit(EXIT_FAILURE);
    }
}

void teardown()
{
    clog << "Tearing down VPN" << endl;

    string err;
    if (execute("/usr/lib/mydatakeeper-vpn/iptables-update", {"remove"}, nullptr, &err) != 0) {
        cerr << "/usr/lib/mydatakeeper-vpn/iptables-update: " << err << endl;
        exit(EXIT_FAILURE);
    }
}

void forkOrReload()
{
    if (pid == 0) {
        clog << "Forking and launching openvpn process" << endl;
        if ((pid = fork()) == 0) {
            const char *args[] = {
                "/usr/bin/openvpn",
                "--suppress-timestamps",
                "--nobind",
                "--config",
                config_file.c_str(),
                (char *)0
            };
            execv(args[0], (char* const*)args);
            // This process is not meant to be exited ever
            perror("execv");
            exit(EXIT_FAILURE);
        } else {
            setup();
        }
    } else {
        clog << "Reloading openvpn process configuration" << endl;
        if (kill(pid, SIGHUP) == -1) {
            perror("kill");
            exit(EXIT_FAILURE);
        }
    }
}

void killIfForked()
{
    if (pid != 0) {
        clog << "Killing openvpn process" << endl;
        if (kill(pid, SIGTERM) == -1) {
            perror("kill");
            exit(EXIT_FAILURE);
        }
        int status;
        if (waitpid(pid, &status, WUNTRACED | WCONTINUED) == -1) {
            perror("wait");
            exit(EXIT_FAILURE);
        }
        pid = 0;
        teardown();
    } else {
        clog << "No openvpn process to kill" << endl;
    }
}

int generateCredentials()
{
    clog << "Writing credentials file" << endl;
    std::ofstream ofs (credentials_file, std::ofstream::out | std::ofstream::trunc);

    ofs << username << endl;
    ofs << password << endl;

    ofs.close();

    std::filesystem::permissions(
        credentials_file,
        std::filesystem::perms::owner_all,
        std::filesystem::perm_options::replace
    );

    return 0;
}

int generateMullvadCA()
{
    clog << "Writing Mullvad CA file" << endl;
    std::ofstream ofs (mullvad_ca_file, std::ofstream::out | std::ofstream::trunc);

    ofs << "-----BEGIN CERTIFICATE-----" << endl;
    ofs << "MIIGIzCCBAugAwIBAgIJAK6BqXN9GHI0MA0GCSqGSIb3DQEBCwUAMIGfMQswCQYD" << endl;
    ofs << "VQQGEwJTRTERMA8GA1UECAwIR290YWxhbmQxEzARBgNVBAcMCkdvdGhlbmJ1cmcx" << endl;
    ofs << "FDASBgNVBAoMC0FtYWdpY29tIEFCMRAwDgYDVQQLDAdNdWxsdmFkMRswGQYDVQQD" << endl;
    ofs << "DBJNdWxsdmFkIFJvb3QgQ0EgdjIxIzAhBgkqhkiG9w0BCQEWFHNlY3VyaXR5QG11" << endl;
    ofs << "bGx2YWQubmV0MB4XDTE4MTEwMjExMTYxMVoXDTI4MTAzMDExMTYxMVowgZ8xCzAJ" << endl;
    ofs << "BgNVBAYTAlNFMREwDwYDVQQIDAhHb3RhbGFuZDETMBEGA1UEBwwKR290aGVuYnVy" << endl;
    ofs << "ZzEUMBIGA1UECgwLQW1hZ2ljb20gQUIxEDAOBgNVBAsMB011bGx2YWQxGzAZBgNV" << endl;
    ofs << "BAMMEk11bGx2YWQgUm9vdCBDQSB2MjEjMCEGCSqGSIb3DQEJARYUc2VjdXJpdHlA" << endl;
    ofs << "bXVsbHZhZC5uZXQwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQCifDn7" << endl;
    ofs << "5E/Zdx1qsy31rMEzuvbTXqZVZp4bjWbmcyyXqvnayRUHHoovG+lzc+HDL3HJV+kj" << endl;
    ofs << "xKpCMkEVWwjY159lJbQbm8kkYntBBREdzRRjjJpTb6haf/NXeOtQJ9aVlCc4dM66" << endl;
    ofs << "bEmyAoXkzXVZTQJ8h2FE55KVxHi5Sdy4XC5zm0wPa4DPDokNp1qm3A9Xicq3Hsfl" << endl;
    ofs << "LbMZRCAGuI+Jek6caHqiKjTHtujn6Gfxv2WsZ7SjerUAk+mvBo2sfKmB7octxG7y" << endl;
    ofs << "AOFFg7YsWL0AxddBWqgq5R/1WDJ9d1Cwun9WGRRQ1TLvzF1yABUerjjKrk89RCzY" << endl;
    ofs << "ISwsKcgJPscaDqZgO6RIruY/xjuTtrnZSv+FXs+Woxf87P+QgQd76LC0MstTnys+" << endl;
    ofs << "AfTMuMPOLy9fMfEzs3LP0Nz6v5yjhX8ff7+3UUI3IcMxCvyxdTPClY5IvFdW7CCm" << endl;
    ofs << "mLNzakmx5GCItBWg/EIg1K1SG0jU9F8vlNZUqLKz42hWy/xB5C4QYQQ9ILdu4ara" << endl;
    ofs << "PnrXnmd1D1QKVwKQ1DpWhNbpBDfE776/4xXD/tGM5O0TImp1NXul8wYsDi8g+e0p" << endl;
    ofs << "xNgY3Pahnj1yfG75Yw82spZanUH0QSNoMVMWnmV2hXGsWqypRq0pH8mPeLzeKa82" << endl;
    ofs << "gzsAZsouRD1k8wFlYA4z9HQFxqfcntTqXuwQcQIDAQABo2AwXjAdBgNVHQ4EFgQU" << endl;
    ofs << "faEyaBpGNzsqttiSMETq+X/GJ0YwHwYDVR0jBBgwFoAUfaEyaBpGNzsqttiSMETq" << endl;
    ofs << "+X/GJ0YwCwYDVR0PBAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQEL" << endl;
    ofs << "BQADggIBADH5izxu4V8Javal8EA4DxZxIHUsWCg5cuopB28PsyJYpyKipsBoI8+R" << endl;
    ofs << "XqbtrLLue4WQfNPZHLXlKi+A3GTrLdlnenYzXVipPd+n3vRZyofaB3Jtb03nirVW" << endl;
    ofs << "Ga8FG21Xy/f4rPqwcW54lxrnnh0SA0hwuZ+b2yAWESBXPxrzVQdTWCqoFI6/aRnN" << endl;
    ofs << "8RyZn0LqRYoW7WDtKpLmfyvshBmmu4PCYSh/SYiFHgR9fsWzVcxdySDsmX8wXowu" << endl;
    ofs << "Ffp8V9sFhD4TsebAaplaICOuLUgj+Yin5QzgB0F9Ci3Zh6oWwl64SL/OxxQLpzMW" << endl;
    ofs << "zr0lrWsQrS3PgC4+6JC4IpTXX5eUqfSvHPtbRKK0yLnd9hYgvZUBvvZvUFR/3/fW" << endl;
    ofs << "+mpBHbZJBu9+/1uux46M4rJ2FeaJUf9PhYCPuUj63yu0Grn0DreVKK1SkD5V6qXN" << endl;
    ofs << "0TmoxYyguhfsIPCpI1VsdaSWuNjJ+a/HIlKIU8vKp5iN/+6ZTPAg9Q7s3Ji+vfx/" << endl;
    ofs << "AhFtQyTpIYNszVzNZyobvkiMUlK+eUKGlHVQp73y6MmGIlbBbyzpEoedNU4uFu57" << endl;
    ofs << "mw4fYGHqYZmYqFaiNQv4tVrGkg6p+Ypyu1zOfIHF7eqlAOu/SyRTvZkt9VtSVEOV" << endl;
    ofs << "H7nDIGdrCC9U/g1Lqk8Td00Oj8xesyKzsG214Xd8m7/7GmJ7nXe5" << endl;
    ofs << "-----END CERTIFICATE-----" << endl;

    ofs.close();

    return 0;
}

int generateConfig()
{
    generateCredentials();
    generateMullvadCA();

    clog << "Writing config file" << endl;
    std::ofstream ofs (config_file, std::ofstream::out | std::ofstream::trunc);

    /* Base parameters */
    ofs << "client" << endl;
    ofs << "dev tun" << endl;
    ofs << "proto " << protocol << endl;

    /* Relays */
    ofs << "remote-random" << endl;

    uint16_t min_weight =  min_element(relays_ip.begin(), relays_ip.end(),
        [](auto& t1, auto& t2) { return t1.second < t2.second; }
    )->second;
    for (auto& it : relays_ip) {
        for (size_t i = 0; i < it.second / min_weight; ++i) {
            ofs << "remote " << it.first << " " << port << endl;
        }
    }

    /* Credentials */
    ofs << "auth-user-pass " << credentials_file << endl;

    /* Security */
    ofs << "cipher AES-256-CBC" << endl;
    ofs << "tls-cipher TLS-DHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA" << endl;

    /* Misc parameters */
    ofs << "resolv-retry infinite" << endl;
    ofs << "nobind" << endl;
    ofs << "persist-key" << endl;
    ofs << "persist-tun" << endl;
    ofs << "verb 3" << endl;
    ofs << "remote-cert-tls server" << endl;
    ofs << "ping 10" << endl;
    ofs << "ping-restart 60" << endl;
    ofs << "sndbuf 524288" << endl;
    ofs << "rcvbuf 524288" << endl;
    ofs << "auth-nocache" << endl;
    ofs << "comp-lzo" << endl;
    if (protocol == "udp")
        ofs << "fast-io" << endl;

    /* Mullvad identity */
    ofs << "ca " << mullvad_ca_file << endl;

    ofs.close();

    return 0;
}

void update_connection_options(auto& relays)
{
    bool compare_ports = false;

    relays_ip.clear();
    udp_ports.clear();
    tcp_ports.clear();

    for (auto& relay : relays) {
        // Add relay IP
        relays_ip[relay._2] = relay._3;

        // Add intersection of available ports
        if (!compare_ports) {
            udp_ports.insert(relay._4.begin(), relay._4.end());
            tcp_ports.insert(relay._5.begin(), relay._5.end());
        } else {
            for (auto& p : udp_ports)
                if (find(relay._4.begin(), relay._4.end(),p) == relay._4.end())
                    udp_ports.erase(p);
            for (auto& p : tcp_ports)
                if (find(relay._5.begin(), relay._5.end(),p) == relay._5.end())
                    tcp_ports.erase(p);
        }
        compare_ports = true;
    }
}

void update_location_field(auto& fields, auto& mullvad)
{
    // Set location options
    string auto_label("Automatique");
    map<string,map<string, string>> location_options;
    auto locations = mullvad.openvpn_relays();
    for (auto& l : locations) {
        location_options[l._2][l._1] = l._3;
        if (location == l._1) {
            update_connection_options(l._6);
            auto_label += " (" + l._3 + ')';
        }
    }

    if (location == "_auto") {
        auto l = mullvad.get_closest_location();
        location = l._1;
        update_connection_options(l._6);
        auto_label += " (" + l._3 + ')';
    }

    map<string,DBus::Variant> options;
    for (auto& it: location_options) {
        options[it.first] = to_variant(it.second);
    }
    options["_auto"] = to_variant(auto_label);
    fields["location"]["options"] = to_variant(options);
}

void update_protocol_field(auto& fields)
{
    map<string,string> options;
    if (!udp_ports.empty()) {
        options["udp"] = "UDP - rapide";
    }
    if (!tcp_ports.empty()) {
        options["tcp"] = "TCP - stable";
    }

    if (protocol == "_auto" || options.find(protocol) == options.end()) {
        protocol = options.rbegin()->first;
    }

    options["_auto"] = "Automatique (" + options[protocol] + ')';
    fields["protocol"]["options"] = to_variant(options);
}

void update_port_field(auto& fields)
{
    map<string, string> options;
    for (auto& p : protocol == "udp" ? udp_ports : tcp_ports) {
        options[to_string(p)] = "Port " + to_string(p);
    }

    if (port == "_auto" || options.find(port) == options.end()) {
        port = options.begin()->first;
    }

    options["_auto"] = "Automatique (" + options[port] + ')';
    fields["port"]["options"] = to_variant(options);
}

void update_fields(auto& vpn, auto& mullvad)
{
    DBus::Variant v = vpn.Get(fr::mydatakeeper::ApplicationInterface, "fields");
    map<string,map<string,DBus::Variant>> fields = v;

    location = vpn.Get(fr::mydatakeeper::ConfigInterface, "location").operator string();
    protocol = vpn.Get(fr::mydatakeeper::ConfigInterface, "protocol").operator string();
    port = vpn.Get(fr::mydatakeeper::ConfigInterface, "port").operator string();

    update_location_field(fields, mullvad);
    update_protocol_field(fields);
    update_port_field(fields);

    vpn.Set(fr::mydatakeeper::ApplicationInterface, "fields", to_variant(fields));
}

void update_account(auto& vpn, auto& mullvad)
{
    username = vpn.Get(fr::mydatakeeper::ConfigInterface, "username").operator string();
    password = vpn.Get(fr::mydatakeeper::ConfigInterface, "password").operator string();
    expiry_date = vpn.Get(fr::mydatakeeper::ConfigInterface, "expiry_date");

    string account_number = mullvad.account_number();
    string account_password = "m";
    uint64_t mullvad_expiry_date = mullvad.expiry_date();

    if (account_number != username) {
        username = account_number;
        vpn.Set(fr::mydatakeeper::ConfigInterface, "username", to_variant(account_number));
    }

    if (account_password != password) {
        password = account_password;
        vpn.Set(fr::mydatakeeper::ConfigInterface, "password", to_variant(account_password));
    }

    if (expiry_date != mullvad_expiry_date) {
        expiry_date = mullvad_expiry_date;
        vpn.Set(fr::mydatakeeper::ConfigInterface, "expiry_date", to_variant(mullvad_expiry_date));
    }
}

void update(auto& vpn, auto& mullvad)
{
    try {
        update_account(vpn, mullvad);
        update_fields(vpn, mullvad);

        if (generateConfig() == 0) {
            uint64_t now = chrono::duration_cast<chrono::seconds>(
                chrono::system_clock::now().time_since_epoch()).count();
            if (expiry_date >= now) {
                forkOrReload();
            } else {
                killIfForked();
            }
        }
    } catch (DBus::Error& e) {
        cerr << "An error occured during VPN config update : " << e.message() << endl;
    }
}

void handler(int sig)
{
    clog << "Stopping DBus main loop (sig " << sig << ')' << endl;
    dispatcher.leave();
    killIfForked();
}

void bus_type(const string& name, const string& value)
{
    if (value != "system" && value != "session")
        throw invalid_argument(name + "must be either 'system' or 'session'");
}

int main(int argc, char** argv)
{
    string bus, bus_name, bus_path, unit, conf_dir;
    mydatakeeper::parse_arguments(argc, argv, VERSION, {
        {
            bus, "bus", 0, mydatakeeper::required_argument, "system", bus_type,
            "The type of bus used by Dbus interfaces. Either system or session"
        },
        {
            bus_name, "bus-name", 0, mydatakeeper::required_argument, fr::mydatakeeper::ApplicationsName, nullptr,
            "The Dbus name which handles the configuration"
        },
        {
            bus_path, "bus-path", 0, mydatakeeper::required_argument, "/fr/mydatakeeper/app/vpn", nullptr,
            "The Dbus path which handles the configuration"
        },
        {
            unit, "unit", 0, mydatakeeper::required_argument, "fr.mydatakeeper.app.vpn.service", nullptr,
            "The name of the unit to handle"
        },
        {
            conf_dir, "conf-dir", 0, mydatakeeper::required_argument, "/var/lib/mydatakeeper-config/apps/fr.mydatakeeper.app.vpn", nullptr,
            "The folder where configuration files will be generated"
        },
    });

    // Setup signal handler
    signal(SIGINT, handler);
    signal(SIGTERM, handler);

    // Setup DBus dispatcher
    DBus::default_dispatcher = &dispatcher;

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    config_file = conf_dir + "/openvpn.conf";
    credentials_file = conf_dir + "/credentials.txt";
    mullvad_ca_file = conf_dir + "/mullvad_ca.crt";
    mullvad_relays_file = conf_dir + "/mullvad_relays.json";

    clog << "Getting Systemd proxy" << endl;
    org::freedesktop::systemd1::ManagerProxy systemd(conn);
    string unit_path = systemd.LoadUnit(unit);

    clog << "Getting Config " << bus_path << " proxy" << endl;
    fr::mydatakeeper::ConfigProxy vpn(conn, bus_path, bus_name);

    clog << "Getting Mullvad Proxy" << endl;
    MullvadProxy mullvad(conn);

    clog << "Setting up Config " << bus_path << " update listener" << endl;
    vpn.onPropertiesChanged = [&vpn, &mullvad](auto& /*iface*/, auto& /*changed*/, auto& /*invalidated*/)
    {
        clog << "VPN config has changed" << endl;
        update(vpn, mullvad);
    };

    clog << "Setting up Mullvad update listener" << endl;
    mullvad.onPropertiesChanged = [&vpn, &mullvad](auto& /*iface*/, auto& /*changed*/, auto& /*invalidated*/)
    {
        clog << "Mullvad config has changed" << endl;
        update(vpn, mullvad);
    };

    // Run forced update at startup
    update(vpn, mullvad);

    clog << "Starting DBus main loop" << endl;
    dispatcher.enter();

    return 0;
}