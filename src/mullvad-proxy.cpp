#include "mullvad-proxy.h"

extern const string MullvadPath("/net/mullvad");
extern const string MullvadName("net.mullvad");

MullvadProxy::MullvadProxy(DBus::Connection &connection)
: DBusProxy(connection, MullvadPath, MullvadName)
{}

