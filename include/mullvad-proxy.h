#ifndef MULLVAD_PROXY_H_INCLUDED
#define MULLVAD_PROXY_H_INCLUDED

#include <mydatakeeper/dbus.h>

#include "net.mullvad.h"

extern const string MullvadPath;
extern const string MullvadName;

class MullvadProxy
: public DBusProxy,
  public net::mullvad_proxy
{
public:
    using ports_t = vector<uint16_t>;
    using relay_t = DBus::Struct<string, string, uint32_t, ports_t, ports_t>;
    using location_t = DBus::Struct<string, string, string, double, double, vector<relay_t>>;
    
    MullvadProxy(DBus::Connection &connection);
    virtual ~MullvadProxy() {}
};

#endif /* MULLVAD_PROXY_H_INCLUDED */
